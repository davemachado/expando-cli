use rand::prelude::thread_rng;
use rand::Rng;

pub fn expand(expand_text: Vec<&str>, sponge: bool, caps: bool) -> String {
    return if sponge {
        sponge_transform(expand_text.join(" "))
    } else {
        let expanded = classic_expand(&expand_text);
        return if caps { to_caps(expanded) } else { expanded };
    };
}

fn classic_expand(expand_text: &Vec<&str>) -> String {
    expand_text
        .join(" ")
        .split("")
        .collect::<Vec<&str>>()
        .join(" ")
        .trim()
        .to_owned()
}

fn to_caps(expand_text: String) -> String {
    let mut capitalized = "".to_owned();
    for e in expand_text.chars() {
        for upper in e.to_uppercase() {
            capitalized.push(upper);
        }
    }

    capitalized
}

fn sponge_transform(to_sponge: String) -> String {
    let mut spongeified = "".to_owned();
    let mut rng = thread_rng();
    for s in to_sponge.chars() {
        let to_cap: f64 = rng.gen();
        if to_cap > 0.5 {
            for upper in s.to_uppercase() {
                spongeified.push(upper);
            }
        } else {
            for lower in s.to_lowercase() {
                spongeified.push(lower);
            }
        }
    }

    return spongeified;
}

#[cfg(test)]
mod tests {

    use crate::expander;

    #[test]
    fn classic_expand_works() {
        let v = vec!["bubber"];
        let result = expander::classic_expand(&v);
        assert_eq!(result, "b u b b e r");
    }
    #[test]
    fn to_caps_works() {
        let result = expander::to_caps("bubber".to_string());
        assert_eq!(result, "BUBBER");
    }
}
