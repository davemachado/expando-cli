use clap;

mod expander;

use expander::expand;

fn main() {
    let args = clap::App::new("Expand text")
        .version("0.0.1")
        .author("Darrien Glasser <me@darrien.dev>")
        .setting(clap::AppSettings::TrailingVarArg)
        .arg(
            clap::Arg::with_name("EXPAND_TEXT")
                .required(true)
                .takes_value(true)
                .multiple(true)
                .help("Text to expand"),
        )
        .arg(
            clap::Arg::with_name("ALL_CAPS")
                .required(false)
                .short("a")
                .long("all-caps")
                .help("Make expanded text all caps"),
        )
        .arg(
            clap::Arg::with_name("SPONGE")
                .required(false)
                .conflicts_with("ALL_CAPS")
                .short("s")
                .long("sponge")
                .help("MaKe TExt SpONgElIkE"),
        )
        .get_matches();

    let user_text: Vec<_> = args.values_of("EXPAND_TEXT").unwrap().collect();
    let all_caps = args.is_present("ALL_CAPS");
    let sponge = args.is_present("SPONGE");

    println!("{}", expand(user_text, sponge, all_caps));
}
